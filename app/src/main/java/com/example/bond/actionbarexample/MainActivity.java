package com.example.bond.actionbarexample;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.Stack;

public class MainActivity extends ActionBarActivity implements ActionBar.OnNavigationListener {
    ImageView img;
    TextView txt;
    final int photoCount = 3;
    int currentPhoto = 0;
    int currentCategory = 0;
    String[] categories;
    int[][] photoScore = new int[photoCount][3];
    ActionBar bar;
    Stack<Integer> history = new Stack<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        img = (ImageView) findViewById(R.id.imageView);
        txt = (TextView) findViewById(R.id.textView);

        bar = getSupportActionBar();
        bar.setDisplayShowTitleEnabled(false);
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        SpinnerAdapter aAdpt = ArrayAdapter.createFromResource(this, R.array.categories, android.R.layout.simple_spinner_dropdown_item);
        bar.setListNavigationCallbacks(aAdpt, this);
        categories = getResources().getStringArray(R.array.categories);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_like:
                ++photoScore[currentPhoto][currentCategory];
                updateLikes();
                return true;
            case R.id.action_unlike:
                --photoScore[currentPhoto][currentCategory];
                updateLikes();
                return true;
            case R.id.action_next:
                showNextPhoto();
                return true;
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                if(!history.empty()) {
                    currentCategory = history.pop();
                    setPhoto(0);
                    bar.setSelectedNavigationItem(currentCategory);
                    if (history.empty())
                        bar.setDisplayHomeAsUpEnabled(false);
                    return true;
                } else
                    return false;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showNextPhoto() {
        if (currentPhoto == photoCount - 1)
            setPhoto(0);
        else
            setPhoto(currentPhoto + 1);
    }


    @Override
    public boolean onNavigationItemSelected(int position, long id) {
        if(currentCategory != position) {
            if (history.empty())
                bar.setDisplayHomeAsUpEnabled(true);
            history.push(currentCategory);
            currentCategory = position;
            setPhoto(0);
        }
        return true;
    }

    private void setPhoto(int number) {
        int imageResource = getResources().getIdentifier(categories[currentCategory].toLowerCase() + number, "drawable", getPackageName());
        img.setImageResource(imageResource);
        currentPhoto = number;
        updateLikes();
    }

    private void updateLikes() {
        txt.setText("Likes: " + photoScore[currentPhoto][currentCategory]);
    }
}
